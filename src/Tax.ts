export class Tax {

  MAX_FREE_ALLOWANCE_TAX = 11000

  HIGH_SALARY_LIMIT = 43000
  HIGHEST_SALARY_LIMIT = 150000

  NORMAL_TAX_RATE = 0.2
  HIGH_TAX_RATE = 0.4
  HIGHEST_TAX_RATE = 0.45

  annualSalary:number
  taxFreeAllowance:number
  taxAbleIncome:number
  taxPayable:number

  constructor(annualSalary:number)
  {
    this.annualSalary = annualSalary
    this.taxFreeAllowance = this.calculateTaxFreeAllowance()
    this.taxAbleIncome = this.calculateTaxableIncome()
    this.taxPayable = this.calculateTaxPayable()
  }
  calculateTaxableIncome = () => {
    return this.annualSalary > this.MAX_FREE_ALLOWANCE_TAX ? this.annualSalary - this.taxFreeAllowance : 0
  }
  calculateTaxFreeAllowance = () => {
    if(this.annualSalary > 100000){
      let exceed = this.annualSalary - 100000
      let decreasedAllowance = exceed / 2
      return decreasedAllowance > this.MAX_FREE_ALLOWANCE_TAX ? 0 : this.MAX_FREE_ALLOWANCE_TAX - decreasedAllowance
    }else if (this.annualSalary > this.MAX_FREE_ALLOWANCE_TAX){
      return this.MAX_FREE_ALLOWANCE_TAX
    }else {
      return this.annualSalary
    }
  }
  calculateTaxPayableNormalRate = () => {
    let taxAmt = 0
    if(this.annualSalary > this.taxFreeAllowance){
      let taxAble = this.annualSalary > this.HIGH_SALARY_LIMIT ? this.HIGH_SALARY_LIMIT : this.annualSalary
      let taxDiff = this.MAX_FREE_ALLOWANCE_TAX - this.taxFreeAllowance
      taxAmt = ((taxAble - taxDiff) - this.taxFreeAllowance) * this.NORMAL_TAX_RATE
    }
    return taxAmt
  }
  calculateTaxPayableHighRate = () => {
    let taxAmt = 0 
    if(this.annualSalary > this.HIGH_SALARY_LIMIT){
      let taxAble = ( this.annualSalary > this.HIGHEST_SALARY_LIMIT ? this.HIGHEST_SALARY_LIMIT : this.annualSalary ) - this.HIGH_SALARY_LIMIT
      let taxDiff = this.MAX_FREE_ALLOWANCE_TAX - this.taxFreeAllowance
      taxAmt = ((taxAble + taxDiff) * this.HIGH_TAX_RATE)
    }
    return taxAmt
  }
  calculateTaxPayableHigherRate = () => {
    let taxAmt = 0
    if(this.annualSalary > this.HIGHEST_SALARY_LIMIT)
    {
      taxAmt = (this.annualSalary - this.HIGHEST_SALARY_LIMIT) * this.HIGHEST_TAX_RATE
    }
    return taxAmt
  }
  calculateTaxPayable = () => {
    return this.calculateTaxPayableNormalRate() + this.calculateTaxPayableHighRate() + this.calculateTaxPayableHigherRate()
  }
  
}