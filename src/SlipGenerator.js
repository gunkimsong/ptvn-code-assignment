"use strict";
exports.__esModule = true;
exports.SlipGenerator = void 0;
var SalarySlip_1 = require("./SalarySlip");
var SlipGenerator = /** @class */ (function () {
    function SlipGenerator() {
        var _this = this;
        this.generateFor = function (employee) {
            _this.salarySlip = new SalarySlip_1.SalarySlip(employee);
            return ("\n      Employee ID : " + employee.getEmployeeID() + "\n      Employee Name : " + employee.getEmployeeName() + "\n      Gross Salary : " + _this.currencyType() + _this.stringToNumber(employee.getMonthlySalary()) + "\n      NationalInsuranceContribution : " + _this.currencyType() + _this.stringToNumber(_this.salarySlip.getNationalInsuraceContribution()) + "\n      Tax-free allowance : " + _this.currencyType() + _this.stringToNumber(_this.salarySlip.getTaxFreeAllowance().toLocaleString()) + "\n      Taxable income: " + _this.currencyType() + _this.stringToNumber(_this.salarySlip.getTaxFreeAllowance().toLocaleString()) + "\n      Taxable Payable: " + _this.currencyType() + _this.stringToNumber(_this.salarySlip.getTaxPayable().toLocaleString()) + "\n      ");
        };
        this.stringToNumber = function (text) {
            return Number(text).toLocaleString();
        };
        this.currencyType = function () {
            return '£';
        };
    }
    return SlipGenerator;
}());
exports.SlipGenerator = SlipGenerator;
