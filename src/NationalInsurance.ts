export class NationalInsurance {
  public MAX_SALARY_NO_CONTRIBUTION = 8060
  public HIGHER_SALARY_CONTRIBUTION = 43000
  public NORMAL_CONTRIBUTION_RATE = 0.12
  public HIGH_CONTRIBUTION_RATE = 0.02

  annualSalary:number
  contribution:number

  constructor(salary:number){
    this.annualSalary = salary
    this.contribution = this.calculateContribution()
  }

  calculateContribution = () =>{
    return this.calculateHighRateContribution() + this.calculateLowRateContribution()
  }
  calculateHighRateContribution = () => {
    let taxAmt = 0
    if(this.annualSalary > this.HIGHER_SALARY_CONTRIBUTION )
    {
      taxAmt = (this.annualSalary - this.HIGHER_SALARY_CONTRIBUTION)
    }
    return taxAmt * this.HIGH_CONTRIBUTION_RATE
  }
  calculateLowRateContribution = () => {
    let taxAmt = 0
    if(this.annualSalary > this.MAX_SALARY_NO_CONTRIBUTION)
    {
      taxAmt = (this.annualSalary > this.HIGHER_SALARY_CONTRIBUTION ? this.HIGHER_SALARY_CONTRIBUTION : this.annualSalary) - this.MAX_SALARY_NO_CONTRIBUTION
    }
    return taxAmt * this.NORMAL_CONTRIBUTION_RATE
  }



}