"use strict";
exports.__esModule = true;
exports.Employee = void 0;
var Employee = /** @class */ (function () {
    function Employee(empId, name, annualSalary) {
        var _this = this;
        this.getEmployeeID = function () {
            return _this.employeeId;
        };
        this.getEmployeeName = function () {
            return _this.employeeName;
        };
        this.getMonthlySalary = function () {
            return (_this.annualSalary / 12).toFixed(2);
        };
        this.employeeId = empId;
        this.employeeName = name;
        this.annualSalary = annualSalary;
    }
    return Employee;
}());
exports.Employee = Employee;
