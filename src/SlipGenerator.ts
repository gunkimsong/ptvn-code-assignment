import { SalarySlip } from "./SalarySlip";
import { Employee } from "./Employee"

export class SlipGenerator {  
  salarySlip:SalarySlip
  generateFor = (employee:Employee) => {
    this.salarySlip = new SalarySlip(employee)
    return (
      `
      Employee ID : ${employee.getEmployeeID()}
      Employee Name : ${employee.getEmployeeName()}
      Gross Salary : ${this.currencyType()}${this.stringToNumber(employee.getMonthlySalary())}
      NationalInsuranceContribution : ${this.currencyType()}${this.stringToNumber(this.salarySlip.getNationalInsuraceContribution())}
      Tax-free allowance : ${this.currencyType()}${this.stringToNumber(this.salarySlip.getTaxFreeAllowance().toLocaleString())}
      Taxable income: ${this.currencyType()}${this.stringToNumber(this.salarySlip.getTaxFreeAllowance().toLocaleString())}
      Taxable Payable: ${this.currencyType()}${this.stringToNumber(this.salarySlip.getTaxPayable().toLocaleString())}
      `
    )
  }
  stringToNumber = (text) => {
    return Number(text).toLocaleString()
  }
  currencyType = () => {
    return '£'
  }
}
