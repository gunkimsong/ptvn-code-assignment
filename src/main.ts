import { Employee } from "./Employee";
import { SlipGenerator } from "./SlipGenerator"

declare var require: any
const fs = require('fs');

var employeeLists = []

fs.readFileSync('input.txt', 'utf8').split(/\r?\n/).forEach((line: any) => {
  let eachLine = line.split(',')
  employeeLists.push({
    id : eachLine[0],
    name : eachLine[1],
    salary : eachLine[2] ? eachLine[2]: 0
  })
})

employeeLists.forEach(item=>{
  let employee = new Employee(item.id,item.name,item.salary)
  let generator = new SlipGenerator()
  console.log(generator.generateFor(employee))
})