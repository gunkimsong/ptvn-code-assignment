export class Employee {
    employeeId: string
    employeeName: string
    annualSalary: number

    constructor(empId:string,name:string,annualSalary:number){
      this.employeeId = empId
      this.employeeName = name
      this.annualSalary = annualSalary
    }

    getEmployeeID = () => {
      return this.employeeId
    }
    getEmployeeName = () => {
      return this.employeeName
    }
    getMonthlySalary = () => {
      return (this.annualSalary/12).toFixed(2)
    }
}