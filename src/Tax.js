"use strict";
exports.__esModule = true;
exports.Tax = void 0;
var Tax = /** @class */ (function () {
    function Tax(annualSalary) {
        var _this = this;
        this.MAX_FREE_ALLOWANCE_TAX = 11000;
        this.HIGH_SALARY_LIMIT = 43000;
        this.HIGHEST_SALARY_LIMIT = 150000;
        this.NORMAL_TAX_RATE = 0.2;
        this.HIGH_TAX_RATE = 0.4;
        this.HIGHEST_TAX_RATE = 0.45;
        this.calculateTaxableIncome = function () {
            return _this.annualSalary > _this.MAX_FREE_ALLOWANCE_TAX ? _this.annualSalary - _this.taxFreeAllowance : 0;
        };
        this.calculateTaxFreeAllowance = function () {
            if (_this.annualSalary > 100000) {
                var exceed = _this.annualSalary - 100000;
                var decreasedAllowance = exceed / 2;
                return decreasedAllowance > _this.MAX_FREE_ALLOWANCE_TAX ? 0 : _this.MAX_FREE_ALLOWANCE_TAX - decreasedAllowance;
            }
            else if (_this.annualSalary > _this.MAX_FREE_ALLOWANCE_TAX) {
                return _this.MAX_FREE_ALLOWANCE_TAX;
            }
            else {
                return _this.annualSalary;
            }
        };
        this.calculateTaxPayableNormalRate = function () {
            var taxAmt = 0;
            if (_this.annualSalary > _this.taxFreeAllowance) {
                var taxAble = _this.annualSalary > _this.HIGH_SALARY_LIMIT ? _this.HIGH_SALARY_LIMIT : _this.annualSalary;
                var taxDiff = _this.MAX_FREE_ALLOWANCE_TAX - _this.taxFreeAllowance;
                taxAmt = ((taxAble - taxDiff) - _this.taxFreeAllowance) * _this.NORMAL_TAX_RATE;
            }
            return taxAmt;
        };
        this.calculateTaxPayableHighRate = function () {
            var taxAmt = 0;
            if (_this.annualSalary > _this.HIGH_SALARY_LIMIT) {
                var taxAble = (_this.annualSalary > _this.HIGHEST_SALARY_LIMIT ? _this.HIGHEST_SALARY_LIMIT : _this.annualSalary) - _this.HIGH_SALARY_LIMIT;
                var taxDiff = _this.MAX_FREE_ALLOWANCE_TAX - _this.taxFreeAllowance;
                taxAmt = ((taxAble + taxDiff) * _this.HIGH_TAX_RATE);
            }
            return taxAmt;
        };
        this.calculateTaxPayableHigherRate = function () {
            var taxAmt = 0;
            if (_this.annualSalary > _this.HIGHEST_SALARY_LIMIT) {
                taxAmt = (_this.annualSalary - _this.HIGHEST_SALARY_LIMIT) * _this.HIGHEST_TAX_RATE;
            }
            return taxAmt;
        };
        this.calculateTaxPayable = function () {
            return _this.calculateTaxPayableNormalRate() + _this.calculateTaxPayableHighRate() + _this.calculateTaxPayableHigherRate();
        };
        this.annualSalary = annualSalary;
        this.taxFreeAllowance = this.calculateTaxFreeAllowance();
        this.taxAbleIncome = this.calculateTaxableIncome();
        this.taxPayable = this.calculateTaxPayable();
    }
    return Tax;
}());
exports.Tax = Tax;
