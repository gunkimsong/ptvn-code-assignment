"use strict";
exports.__esModule = true;
var Employee_1 = require("./Employee");
var SlipGenerator_1 = require("./SlipGenerator");
var fs = require('fs');
var employeeLists = [];
fs.readFileSync('input.txt', 'utf8').split(/\r?\n/).forEach(function (line) {
    var eachLine = line.split(',');
    employeeLists.push({
        id: eachLine[0],
        name: eachLine[1],
        salary: eachLine[2] ? eachLine[2] : 0
    });
});
employeeLists.forEach(function (item) {
    var employee = new Employee_1.Employee(item.id, item.name, item.salary);
    var generator = new SlipGenerator_1.SlipGenerator();
    console.log(generator.generateFor(employee));
});
