"use strict";
exports.__esModule = true;
exports.SalarySlip = void 0;
var NationalInsurance_1 = require("./NationalInsurance");
var Tax_1 = require("./Tax");
var SalarySlip = /** @class */ (function () {
    function SalarySlip(employee) {
        var _this = this;
        this.getNationalInsuraceContribution = function () {
            return (_this.nationalInsurance.contribution / 12).toFixed(2);
        };
        this.employee = employee;
        this.nationalInsurance = new NationalInsurance_1.NationalInsurance(this.employee.annualSalary);
        this.taxInfo = new Tax_1.Tax(this.employee.annualSalary);
    }
    SalarySlip.prototype.getTaxAbleIncome = function () {
        return (this.taxInfo.taxAbleIncome / 12).toFixed(2);
    };
    SalarySlip.prototype.getTaxFreeAllowance = function () {
        return (this.taxInfo.taxFreeAllowance / 12).toFixed(2);
    };
    SalarySlip.prototype.getTaxPayable = function () {
        return (this.taxInfo.taxPayable / 12).toFixed(2);
    };
    return SalarySlip;
}());
exports.SalarySlip = SalarySlip;
