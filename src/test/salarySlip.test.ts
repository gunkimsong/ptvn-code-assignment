import { Employee } from "../Employee"
import { SalarySlip } from "../SalarySlip";

describe("SalarySlip", () => {
  const employee = new Employee('1234', 'TEST_EMPLOYEE', 1234);
  const employee_1 = new Employee('12345', 'TEST_1', 5000);
  const employee_2 = new Employee('12347', 'TEST_2', 9060);
  const employee_3 = new Employee('12587', 'TEST_3', 12000);
  const employee_4 = new Employee('12311', 'TEST_4', 160000);
  const employee_5 = new Employee('14785', 'TEST_5', 122000);

  const salarySlip = new SalarySlip(employee)
  const salarySlip_1 = new SalarySlip(employee_1)
  const salarySlip_2 = new SalarySlip(employee_2)
  const salarySlip_3 = new SalarySlip(employee_3)
  const salarySlip_4 = new SalarySlip(employee_4)
  const salarySlip_5 = new SalarySlip(employee_5)

  it('can create instance',()=>{
    expect(salarySlip.employee.employeeId).toEqual('1234');
    expect(salarySlip.employee.employeeName).toEqual('TEST_EMPLOYEE');
    expect(salarySlip.employee.annualSalary).toEqual(1234);
  })

  // Any amount of money earned above a gross annual salary of £8,060.00 is subject to a National Insurance contribution of 12%
  // National Insurance (higher contributions): Any amount of money earned above a gross annual salary of £43,000.00 is only subject to a 2% NI contribution

  it('national insurance contribution calculate', ()=>{
    expect(salarySlip.getNationalInsuraceContribution()).toEqual('0.00')
    expect(salarySlip_1.getNationalInsuraceContribution()).toEqual('0.00')
    expect(salarySlip_2.getNationalInsuraceContribution()).toEqual('10.00')
    expect(salarySlip_3.getNationalInsuraceContribution()).toEqual('39.40')
    expect(salarySlip_4.getNationalInsuraceContribution()).toEqual('544.40')
    expect(salarySlip_5.getNationalInsuraceContribution()).toEqual('481.07')
  })

  // Taxable income: Any amount of money earned above a gross annual salary of £11,000.00 is taxed at 20%
  // Taxable income (higher rate): Any amount of money earned above a gross annual salary of £43,000.00 is taxed at 40%
  // When the Annual Gross Salary exceeds £100,000.00, the tax-free allowance starts decreasing. 
  // It decreases by £1 for every £2 earned over £100,000.00. And this
  // excess is taxed at the Higher rate tax.

  it('tax-free allowance calculate', ()=> {
    expect(salarySlip.getTaxFreeAllowance()).toEqual('102.83')
    expect(salarySlip_1.getTaxFreeAllowance()).toEqual('416.67')
    expect(salarySlip_2.getTaxFreeAllowance()).toEqual('755.00')
    expect(salarySlip_3.getTaxFreeAllowance()).toEqual('916.67')
    expect(salarySlip_4.getTaxFreeAllowance()).toEqual('0.00')
    expect(salarySlip_5.getTaxFreeAllowance()).toEqual('0.00')
  })

  it('taxable income calculate', ()=> {
    expect(salarySlip.getTaxAbleIncome()).toEqual('0.00')
    expect(salarySlip_1.getTaxAbleIncome()).toEqual('0.00')
    expect(salarySlip_2.getTaxAbleIncome()).toEqual('0.00')
    expect(salarySlip_3.getTaxAbleIncome()).toEqual('83.33')
    expect(salarySlip_4.getTaxAbleIncome()).toEqual('13333.33')
    expect(salarySlip_5.getTaxAbleIncome()).toEqual('10166.67')
  })

  it('taxable payable calculate', ()=> {
    expect(salarySlip.getTaxPayable()).toEqual('0.00')
    expect(salarySlip_1.getTaxPayable()).toEqual('0.00')
    expect(salarySlip_2.getTaxPayable()).toEqual('0.00')
    expect(salarySlip_3.getTaxPayable()).toEqual('16.67')
    expect(salarySlip_4.getTaxPayable()).toEqual('4841.67')
    expect(salarySlip_5.getTaxPayable()).toEqual('3533.33')
  })

});