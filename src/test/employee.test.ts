import { Employee } from "../Employee";

describe("Employee", () => {
  const employee = new Employee('1234', 'GUN', 1234);
  it("employee can explain self",()=> {
    expect(employee.getEmployeeID()).toEqual('1234')
    expect(employee.getEmployeeName()).toEqual('GUN')
  });
  it("calculate employee monthly salary",()=>{
    expect(employee.getMonthlySalary()).toEqual((employee.annualSalary/12).toFixed(2))
  })
});