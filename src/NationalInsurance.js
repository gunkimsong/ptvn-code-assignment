"use strict";
exports.__esModule = true;
exports.NationalInsurance = void 0;
var NationalInsurance = /** @class */ (function () {
    function NationalInsurance(salary) {
        var _this = this;
        this.MAX_SALARY_NO_CONTRIBUTION = 8060;
        this.HIGHER_SALARY_CONTRIBUTION = 43000;
        this.NORMAL_CONTRIBUTION_RATE = 0.12;
        this.HIGH_CONTRIBUTION_RATE = 0.02;
        this.calculateContribution = function () {
            return _this.calculateHighRateContribution() + _this.calculateLowRateContribution();
        };
        this.calculateHighRateContribution = function () {
            var taxAmt = 0;
            if (_this.annualSalary > _this.HIGHER_SALARY_CONTRIBUTION) {
                taxAmt = (_this.annualSalary - _this.HIGHER_SALARY_CONTRIBUTION);
            }
            return taxAmt * _this.HIGH_CONTRIBUTION_RATE;
        };
        this.calculateLowRateContribution = function () {
            var taxAmt = 0;
            if (_this.annualSalary > _this.MAX_SALARY_NO_CONTRIBUTION) {
                taxAmt = (_this.annualSalary > _this.HIGHER_SALARY_CONTRIBUTION ? _this.HIGHER_SALARY_CONTRIBUTION : _this.annualSalary) - _this.MAX_SALARY_NO_CONTRIBUTION;
            }
            return taxAmt * _this.NORMAL_CONTRIBUTION_RATE;
        };
        this.annualSalary = salary;
        this.contribution = this.calculateContribution();
    }
    return NationalInsurance;
}());
exports.NationalInsurance = NationalInsurance;
