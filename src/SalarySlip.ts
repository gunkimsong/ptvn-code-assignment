import { Employee } from "./Employee";
import { NationalInsurance } from "./NationalInsurance"
import { Tax } from "./Tax"

export class SalarySlip {
  employee : Employee
  nationalInsurance : NationalInsurance
  taxInfo : Tax
  constructor(employee:Employee){
    this.employee = employee
    this.nationalInsurance = new NationalInsurance(this.employee.annualSalary)
    this.taxInfo = new Tax(this.employee.annualSalary)
  }
  getNationalInsuraceContribution = () => {
    return (this.nationalInsurance.contribution/12).toFixed(2)
  }
  getTaxAbleIncome(){
    return (this.taxInfo.taxAbleIncome/12).toFixed(2)
  }
  getTaxFreeAllowance(){
    return (this.taxInfo.taxFreeAllowance/12).toFixed(2)
  }
  getTaxPayable(){
    return (this.taxInfo.taxPayable/12).toFixed(2)
  }
}