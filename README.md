================ HOW TO RUN ==================

1. this project requires nodejs (javascript runtime) to run
- Download at https://nodejs.org/en/
- Recommend version 10+

2. Before the run project, you must install the required package
- type "npm -i" in command-line,terminal at root project

3. UnitTest run "npm run test" at root project

4. Run project 
- "npm run start-javascript" at root project
- OR "npm run start-typescript" at root project
- input.txt is input data. Feel free to edit